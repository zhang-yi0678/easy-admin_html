package com.mars.router;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;


/**
 * 路由控制器
 *
 * @author 源码字节-程序员Mars
 */
@Controller
public class RouterController {

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/home/home.html")
    public String home() {
        return "home/home";
    }

    @GetMapping("/info/info.html")
    public String info() {
        return "info/info";
    }

    @GetMapping("/home/userInfo.html")
    public String userInfo() {
        return "home/userInfo";
    }

    @GetMapping("/home/update-password.html")
    public String updatePassword() {
        return "home/update-password";
    }

    @GetMapping("/log/api.html")
    public String log() {
        return "log/api";
    }

    @GetMapping("/system/sysMenu/menu.html")
    public String menu() {
        return "system/sysMenu/menu";
    }

    @GetMapping("/system/sysMenu/menuAdd.html")
    public String menuAdd() {
        return "system/sysMenu/menuAdd";
    }

    @GetMapping("/system/sysMenu/menuUpdate.html")
    public String menuUpdate() {
        return "system/sysMenu/menuUpdate";
    }

    @GetMapping("/system/sysRole/role.html")
    public String role() {
        return "system/sysRole/role";
    }

    @GetMapping("/system/sysRole/roleAdd.html")
    public String roleAdd() {
        return "system/sysRole/roleAdd";
    }

    @GetMapping("/system/sysRole/roleUpdate.html")
    public String roleUpdate() {
        return "system/sysRole/roleUpdate";
    }

    @GetMapping("/tool/api.html")
    public String toolApi() {
        return "tool/api";
    }

    @GetMapping("/gen/import-table.html")
    public String genImportTable() {
        return "gen/import-table";
    }

    @GetMapping("/gen/import-table-update.html")
    public String genImportTableUpdate() {
        return "gen/import-table-update";
    }

    @GetMapping("/gen/table.html")
    public String tableList() {
        return "gen/table";
    }

    @GetMapping("/system/sysUser/user.html")
    public String user() {
        return "system/sysUser/user";
    }

    @GetMapping("/system/sysUser/userAdd.html")
    public String userAdd() {
        return "system/sysUser/userAdd";
    }

    @GetMapping("/system/sysUser/userUpdate.html")
    public String userUpdate() {
        return "system/sysUser/userUpdate";
    }

    @GetMapping("/system/sysUser/userImport.html")
    public String userImport() {
        return "system/sysUser/userImport";
    }

    @GetMapping("/system/sysPost/post.html")
    public String post() {
        return "system/sysPost/post";
    }

    @GetMapping("/system/sysPost/post-add-update.html")
    public String postAddUpdate() {
        return "system/sysPost/post-add-update";
    }

    @GetMapping("/resourceInfo/resourceInfo.html")
    public String resourceInfo() {
        return "resourceInfo/resourceInfo";
    }

    @GetMapping("/resourceInfo/resourceInfo-add-update.html")
    public String resourceInfoAddUpdate() {
        return "resourceInfo/resourceInfo-add-update";
    }

    @GetMapping("/gen/create-table.html")
    public String createTable() {
        return "gen/create-table";
    }

    @GetMapping("/gen/code-remove.html")
    public String codeRemove() {
        return "gen/code-remove";
    }

    @GetMapping("/system/sysLoginRecord/sysLoginRecord.html")
    public String sysLoginRecord() {
        return "system/sysLoginRecord/sysLoginRecord";
    }

    @GetMapping("/system/sysLoginRecord/sysLoginRecord-add-update.html")
    public String sysLoginRecordAddUpdate() {
        return "system/sysLoginRecord/sysLoginRecord-add-update";
    }

    @GetMapping("/system/sysOperLog/sysOperLog.html")
    public String sysOperLog() {
        return "system/sysOperLog/sysOperLog";
    }

    @GetMapping("/system/sysOperLog/sysOperLog-add-update.html")
    public String sysOperLogAddUpdate() {
        return "system/sysOperLog/sysOperLog-add-update";
    }

    @GetMapping("/system/sysDictType/sysDictType.html")
    public String sysDictType() {
        return "system/sysDictType/sysDictType";
    }

    @GetMapping("/system/sysDictType/sysDictType-add-update.html")
    public String sysDictTypeAddUpdate() {
        return "system/sysDictType/sysDictType-add-update";
    }

    @GetMapping("/system/sysDictData/sysDictData.html")
    public String sysDictData() {
        return "system/sysDictData/sysDictData";
    }

    @GetMapping("/system/sysDictData/sysDictData-add-update.html")
    public String sysDictDataAddUpdate() {
        return "system/sysDictData/sysDictData-add-update";
    }

    @GetMapping("/system/sysConfig/sysConfig.html")
    public String sysConfig() {
        return "system/sysConfig/sysConfig";
    }

    @GetMapping("/system/sysConfig/sysConfig-add-update.html")
    public String sysConfigAddUpdate() {
        return "system/sysConfig/sysConfig-add-update";
    }

    @GetMapping("/system/sysOss/sysOss.html")
    public String sysOss() {
        return "system/sysOss/sysOss";
    }

    @GetMapping("/system/sysOss/sysOss-add-update.html")
    public String sysOssAddUpdate() {
        return "system/sysOss/sysOss-add-update";
    }

    @GetMapping("/admin/apTest/apTest.html")
    public String adminApTest() {
        return "admin/apTest/apTest";
    }

    @GetMapping("/admin/apTest/apTestAddUpdate.html")
    public String adminApTestAddUpdate() {
        return "admin/apTest/apTestAddUpdate";
    }

    @GetMapping("/admin/apTest1/apTest1.html")
    public String adminApTest1() {
        return "admin/apTest1/apTest1";
    }

    @GetMapping("/admin/apTest1/apTest1AddUpdate.html")
    public String adminApTest1AddUpdate() {
        return "admin/apTest1/apTest1AddUpdate";
    }

    @GetMapping("/admin/sysNotify/sysNotify.html")
    public String adminSysNotify() {
        return "admin/sysNotify/sysNotify";
    }

    @GetMapping("/admin/sysNotify/sysNotifyAddUpdate.html")
    public String adminSysNotifyAddUpdate() {
        return "admin/sysNotify/sysNotifyAddUpdate";
    }

    @GetMapping("/admin/sysNotify/mineSysNotify.html")
    public String mineSysNotify() {
        return "admin/sysNotify/mineSysNotify";
    }

    @GetMapping("/admin/sysMessage/sysMessage.html")
    public String adminSysMessage() {
        return "admin/sysMessage/sysMessage";
    }

    @GetMapping("/admin/sysMessage/sysMessageAddUpdate.html")
    public String adminSysMessageAddUpdate() {
        return "admin/sysMessage/sysMessageAddUpdate";
    }

    @GetMapping("/system/sysMonitor/sysOnLineUser.html")
    public String sysOnLineUser() {
        return "system/sysMonitor/sysOnLineUser";
    }

    @GetMapping("/workflow/modelList.html")
    public String modelList() {
        return  "workflow/modelList";
    }

    @GetMapping("/workflow/modelAddUpdate.html")
    public String modelAddUpdate() {
        return "workflow/modelAddUpdate";
    }


}
