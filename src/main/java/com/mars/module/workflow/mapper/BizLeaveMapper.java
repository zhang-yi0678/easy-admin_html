package com.mars.module.workflow.mapper;

import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.workflow.entity.BizLeave;


/**
 * 请假业务Mapper
 *
 * @author 程序员Mars
 * @date 2023-10-11
 */
public interface BizLeaveMapper extends BasePlusMapper<BizLeave> {

}
