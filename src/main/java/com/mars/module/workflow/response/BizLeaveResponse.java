package com.mars.module.workflow.response;

import com.mars.module.workflow.entity.BizLeave;
import lombok.Data;

import java.util.Date;

/**
 * @author Hp
 */
@Data
public class BizLeaveResponse extends BizLeave {

    /**
     * 申请人姓名
     */
    private String applyUserName;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 办理时间
     */
    private Date doneTime;

    /**
     * 创建人
     */
    private String createUserName;

    /**
     * 流程实例状态 1 激活 2 挂起
     */
    private String suspendState;

}
