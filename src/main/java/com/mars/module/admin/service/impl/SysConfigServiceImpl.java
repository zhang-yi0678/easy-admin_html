package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import com.mars.common.util.RSAUtils;
import com.mars.module.admin.response.SysConfigResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.SysConfigRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.SysConfigMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.SysConfig;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.ISysConfigService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 系统配置业务层处理
 *
 * @author mars
 * @date 2023-11-20
 */
@Slf4j
@Service
public class SysConfigServiceImpl implements ISysConfigService {

    @Resource
    private SysConfigMapper baseMapper;

    @Value("${easy.admin.apiEncrypt}")
    private boolean apiEncrypt;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysConfig add(SysConfigRequest request) {
        SysConfig entity = new SysConfig();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(SysConfigRequest request) {
        SysConfig entity = new SysConfig();
        BeanUtils.copyProperties(request, entity);
        baseMapper.updateById(entity);
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public SysConfig getById(Long id) {
        return baseMapper.selectById(id);
    }


    @Override
    public SysConfigResponse sysLoginConfig(List<String> configList) {
        List<String> list = new ArrayList<>();
        if (CollectionUtils.isEmpty(configList)) {
            list = Arrays.asList("sys_name", "sys_copyright", "project_slogan");
        }
        List<SysConfig> sysConfigs = baseMapper.selectList(Wrappers.lambdaQuery(SysConfig.class).in(SysConfig::getFieldKey, list));
        if (CollectionUtils.isNotEmpty(sysConfigs)) {
            SysConfigResponse response = new SysConfigResponse();
            SysConfig sysConfig = sysConfigs.stream().filter(x -> "sys_name".equals(x.getFieldKey())).findFirst().get();
            response.setSystemName(sysConfig.getFieldValue());
            SysConfig sysConfigCopyright = sysConfigs.stream().filter(x -> "sys_copyright".equals(x.getFieldKey())).findFirst().get();
            response.setCopyright(sysConfigCopyright.getFieldValue());
            SysConfig slogan = sysConfigs.stream().filter(x -> "project_slogan".equals(x.getFieldKey())).findFirst().get();
            response.setSlogan(slogan.getFieldValue());
            if (apiEncrypt) {
                response.setPublicKey(RSAUtils.getPublicKeyString());
            }
            return response;
        }
        return null;
    }

    @Override
    public PageInfo<SysConfig> pageList(SysConfigRequest request) {
        Page<SysConfig> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<SysConfig> query = this.buildWrapper(request);
        IPage<SysConfig> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }

    private LambdaQueryWrapper<SysConfig> buildWrapper(SysConfigRequest param) {
        LambdaQueryWrapper<SysConfig> query = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(param.getName())) {
            query.like(SysConfig::getName, param.getName());
        }
        if (StringUtils.isNotBlank(param.getFieldKey())) {
            query.like(SysConfig::getFieldKey, param.getFieldKey());
        }
        if (StringUtils.isNotBlank(param.getFieldValue())) {
            query.like(SysConfig::getFieldValue, param.getFieldValue());
        }
        return query;
    }

}
