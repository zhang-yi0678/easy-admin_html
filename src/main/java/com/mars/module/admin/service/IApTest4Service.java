package com.mars.module.admin.service;

import com.mars.module.admin.entity.ApTest4;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.ApTest4Request;

import java.util.List;

/**
 * 测试4接口
 *
 * @author mars
 * @date 2023-12-22
 */
public interface IApTest4Service {
    /**
     * 新增
     *
     * @param param param
     * @return ApTest4
     */
    ApTest4 add(ApTest4Request param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(ApTest4Request param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return ApTest4
     */
    ApTest4 getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<ApTest4>
     */
    PageInfo<ApTest4> pageList(ApTest4Request param);


    /**
     * 查询所有数据
     *
     * @return List<ApTest4>
     */
    List<ApTest4> list();
}
