package com.mars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Easy-Admin 启动类
 *
 * @author 源码字节-程序员Mars
 */
@SpringBootApplication(exclude = {org.activiti.spring.boot.SecurityAutoConfiguration.class,org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
public class EasyAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyAdminApplication.class, args);
    }
}
