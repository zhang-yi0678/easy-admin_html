package com.mars.common.request;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 分页查询DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class PageRequest {


    @ApiModelProperty(value = "当前页")
    private int pageNo = 1;

    @ApiModelProperty(value = "每页记录数")
    private int pageSize = 10;


    public final <T> IPage<T> page() {
        return new Page<>(pageNo, pageSize);
    }


}
