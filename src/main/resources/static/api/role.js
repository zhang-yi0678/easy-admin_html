/**
 * 角色分页列表
 * @param query
 * @returns {*}
 */
function pageList(query) {
    return requests({
        url: '/sys/role/list',
        method: 'post',
        data: query
    })
}
