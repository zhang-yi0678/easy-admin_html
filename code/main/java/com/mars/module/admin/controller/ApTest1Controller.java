package com.mars.module.admin.controller;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.util.ExcelUtils;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import com.mars.module.admin.entity.ApTest1;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.IApTest1Service;
import com.mars.module.admin.request.ApTest1Request;

import javax.servlet.http.HttpServletResponse;

/**
 * 测试1控制层
 *
 * @author mars
 * @date 2024-01-06
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "测试1接口管理",tags = "测试1接口管理")
@RequestMapping("/admin/apTest1" )
public class ApTest1Controller {

    private final IApTest1Service iApTest1Service;

    /**
     * 分页查询测试1列表
     */
    @ApiOperation(value = "分页查询测试1列表")
    @PostMapping("/pageList")
    public R<PageInfo<ApTest1>> pageList(@RequestBody ApTest1Request apTest1Request) {
        return R.success(iApTest1Service.pageList(apTest1Request));
    }

    /**
     * 获取测试1详细信息
     */
    @ApiOperation(value = "获取测试1详细信息")
    @GetMapping(value = "/query/{id}")
    public R<ApTest1> detail(@PathVariable("id") Long id) {
        return R.success(iApTest1Service.getById(id));
    }

    /**
     * 新增测试1
     */
    @Log(title = "新增测试1", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增测试1")
    @PostMapping("/add")
    public R<Void> add(@RequestBody ApTest1Request apTest1Request) {
        iApTest1Service.add(apTest1Request);
        return R.success();
    }

    /**
     * 修改测试1
     */
    @Log(title = "修改测试1", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改测试1")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody ApTest1Request apTest1Request) {
        iApTest1Service.update(apTest1Request);
        return R.success();
    }

    /**
     * 删除测试1
     */
    @Log(title = "删除测试1", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除测试1")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Long[] ids) {
        iApTest1Service.deleteBatch(Arrays.asList(ids));
        return R.success();
    }

    /**
    * 导出测试1
    *
    * @param response response
    * @throws IOException IOException
    */
    @GetMapping(value = "/export")
    public void exportExcel(HttpServletResponse response,@RequestBody ApTest1Request apTest1Request) throws IOException {
        List<ApTest1> list = iApTest1Service.list(apTest1Request);
        ExcelUtils.exportExcel(list, ApTest1.class, "测试1信息", response);
    }
}
